﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarComparator.Models
{
    class Vehicle
    {
        public String make;
        public String model;
        public Dictionary<String, String> properties;
        public Bitmap featuredImage;
        public int year
        {
            set { if (value > 0) { year = value; } }
        }

        public void addProperty(String name, String value)
        {
            if (properties == null)
            {
                properties = new Dictionary<string, string>(10);
            }

            properties.Add(name, value);
        }



    }
}
